#Eduardo Ortiz Alvarez 
#801-11-5100
#eduardo3991@hotmail.com
#Usa Python 3.2.2

def sumatoria(n):
	if n == 1:
		return n
	return n + sumatoria(n - 1)
	
def fibonacci(n):
	if n <= 1:
		return n
	return fibonacci(n - 1) + fibonacci(n - 2)
	
def bah(a, b):
	if len(a) == 0:
		print (b)
		return
	for i in range(len(a)):
		bah(a[:i] + a[i+1:], b + a[i])
		
def sumaLista(L):
	if len(L) == 0:
		return 0
	return L[0] + sumaLista(L[1:len(L)])

def buscaEnLista(L, a):
	if len(L) == 0:
		return -1
	if L[len(L) - 1] == a:
		return len(L) - 1
	else:
		return buscaEnLista(L[0:len(L) - 1], a)
	
def recorre(L):
	if L == None:
		return
	print (L[0])
	L = L[1]
	recorre(L)
	
def buscaEnArbol(tree, key):
	if tree == None:
		return False
	if tree[0] == key:
		return True
	elif tree[0] < key:
		tree = tree[1]
		tree = tree[1]
		return buscaEnArbol(tree, key)
	else:
		tree = tree[1]
		tree = tree[0]
		return buscaEnArbol(tree, key)

	
print ("Sumatoria de 1 a 5 es", sumatoria(5))

print ("6th Fibonacci:", fibonacci(6))

print ("Permutations!")
bah("123", "") #permutations!

LE1 = [1, 2, 3, 4, 5]
print("Summing the List: ", LE1, sumaLista(LE1))

LE2 = [2,3,4,10,40]
print ("Looking for 10 on: ", LE2,  buscaEnLista(LE2, 10))  # imprime 3

print ("Looking for 15 on: ", LE2,  buscaEnLista(LE2, 15))  # imprime -1

a = (5, None)
b = (8, a)
front = (15, b)

x = (1, (2,3), (4, 5, 6), (7, 8, 9, 10))
print ("Recorre front: ", front)
recorre(front)

#recorre((10, (34, (2, None))))
#recorre(x)

#B = ( 10, ( ( 5, ( 2, (None,None) ), ( 8, (None,None) ) ) ) , ( 24, (None, ( 30, (None,None) )) ) )
B = ( 10, ( ( 5, (( 2, (None,None) ), ( 8, (None,None) ) ) ) , ( 24, (None, ( 30, (None,None) )) ) ) )

print ("Looking for 39 on ", B, buscaEnArbol(B, 39))

print ("Looking for 2 on ", B, buscaEnArbol(B, 2))


def DFS(G,n):
    V = [False] * len(G)
    DFSRec(G,n,V)

def DFSRec(G,n,V):
    print (n)
    V[n] = True
    for e in G[n]:
        if V[e] == False:
            DFSRec(G,e,V)


G = [ [], [2,5], [1,3,5], [2,4], [3,5,6], [1,2,4], [4] ]
DFS(G,6)


# Search for the letter and return
# its position as a tuple
def search(M,letter):
    for i in range(len(M)):
        for j in range(len(M[0])):
            if M[i][j] == letter:
                return [i, j]
    return [-1, -1]

# Given a string representing the maze
# return it as a list of lists.
def splitToListOfLists(st):
    M =  st.split("\n")
    L = []
    for line in M:
        LL = []
        for c in line:
            LL.append(c)
        L.append(LL)
    return L

# prints the list of lists as a string
def printLoLAsString(M):
    st = ""
    for i in M:
        st = st + "".join(i) + "\n"
    print (st)

def fill(M, pos):
	if M[pos[0]][pos[1]] == "W":
		return
		
	M[pos[0]][pos[1]] = "P"
	
	if M[pos[0]][pos[1] + 1] != "P":
		fill(M, [pos[0], pos[1] + 1])
	if M[pos[0]][pos[1] - 1] != "P":
		fill(M, [pos[0], pos[1] - 1])
	if M[pos[0] + 1][pos[1]] != "P":
		fill(M, [pos[0] + 1, pos[1]])
	if M[pos[0] - 1][pos[1]] != "P":
		fill(M, [pos[0] - 1, pos[1]])
		
	

	
maze1 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W               W     W\n\
W P  W          W     W\n\
W    W          W     W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

maze2 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W    W          WWWWWWW\n\
W P  WWWWWWW WWWW     W\n\
W        W            W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

maze3 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W    W   P      WWWWWWW\n\
W    WWWWWWW WWWW     W\n\
W        W            W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

maze4 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W  P  W\n\
W    W          WWWWWWW\n\
W    WWWWWWW WWWW     W\n\
W        W            W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

maze5 = "\
WWWWWWWWWWWWWWWWWWWWWWW\n\
W    W          W     W\n\
W               W     W\n\
W    W          W   P W\n\
W    W          W     W\n\
WWWWWWWWWWWWWWWWWWWWWWW"

M1 = splitToListOfLists(maze1)
M2 = splitToListOfLists(maze2)
M3 = splitToListOfLists(maze3)
M4 = splitToListOfLists(maze4)
M5 = splitToListOfLists(maze5)

#print (M1)
#print ("P in maze1 is in:", M1[3,2])

printLoLAsString(M1)
fill(M1, search(M1, "P"))
printLoLAsString(M1)

printLoLAsString(M2)
fill(M2, search(M2, "P"))
printLoLAsString(M2)

printLoLAsString(M3)
fill(M3, search(M3, "P"))
printLoLAsString(M3)

printLoLAsString(M4)
fill(M4, search(M4, "P"))
printLoLAsString(M4)

printLoLAsString(M5)
fill(M5, search(M5, "P"))
printLoLAsString(M5)